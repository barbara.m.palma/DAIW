<?php 
include("conexion.php");
$id_album=intval($_GET['id']);
?>
<!DOCTYPE html>
<html lang="en" class="app">
<head>
<meta charset="utf-8" />
<title>Álbum</title>
<meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link rel="stylesheet" href="css/app.v1.css" type="text/css" />
<!--[if lt IE 9]> <script src="js/ie/html5shiv.js"></script> <script src="js/ie/respond.min.js"></script> <script src="js/ie/excanvas.js"></script> <![endif]-->
    <link rel="shortcut icon" type="image/png" href="images/favicon.png"/>
</head>
<body class="">
<section class="vbox">
  <header class="bg-white header header-md navbar navbar-fixed-top-xs box-shadow">
   <!-- menú superior  -->
        <div class="navbar-header aside-md dk"><a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen"
                                                  data-target="#nav"> <i class="fa fa-bars"></i> </a>
            <a href="index.php" class="navbar-brand"><img src="images/logo.png" class="m-r-sm">WikiMusic</a> <a
                class="btn btn-link visible-xs" data-toggle="dropdown" data-target=".user"> <i class="fa fa-cog"></i>
        </a></div>
        <ul class="nav navbar-nav hidden-xs">
            <!-- menú de puntitos desplegable -->
        </ul>
        <!-- fin menú de puntitos desplegable -->
        <form class="navbar-form navbar-left input-s-lg m-t m-l-n-xs hidden-xs" role="search">
            <!-- buscador -->
            <div class="form-group">
                <div class="input-group"> <span class="input-group-btn">
          <button type="submit" class="btn btn-sm bg-white b-white btn-icon"><i class="fa fa-search"></i></button>
          </span>
                    <input type="text" class="form-control input-sm no-border" placeholder="Busca artistas, estilos...">
                </div>
            </div>
        </form>
        <!-- fin buscador -->
        <ul class="nav navbar-nav navbar-right m-n hidden-xs nav-user user">
            <!-- notificaciones desplegables y perfil -->
            <li class="hidden-xs">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="i i-chat3"></i>
                    <span class="badge badge-sm up bg-danger count">3</span>
                </a>
                <section class="dropdown-menu aside-xl animated flipInY">
                    <section class="panel bg-white">
                        <header class="panel-heading b-light bg-light">
                            <strong>Tienes <span class="count">3</span> notificaciones</strong>
                        </header>
                        <div class="list-group list-group-alt">
                            <a href="#" class="media list-group-item">
                                    <span class="media-body block m-b-none">¡Dua Lipa ha subido un nuevo video!<br>
                                        <small class="text-muted">Hace 10 minutos</small>
                                    </span>
                            </a>
                            <a href="#" class="media list-group-item">
                                    <span class="media-body block m-b-none">Avicii ha anunciado un nuevo concierto<br>
                                        <small class="text-muted">Hace 1 hora</small>
                                    </span>
                            </a>
                            <a href="#" class="media list-group-item">
                                    <span class="media-body block m-b-none">¡Dua Lipa ha subido un nuevo video!<br>
                                        <small class="text-muted">Hace 10 minutos</small>
                                    </span>
                            </a>
                            <a href="#" class="media list-group-item">
                                    <span class="media-body block m-b-none">¡Dua Lipa ha subido un nuevo video!<br>
                                        <small class="text-muted">Hace 10 minutos</small>
                                    </span>
                            </a>
                        </div>
                        <footer class="panel-footer text-sm">
                            <a href="#" class="pull-right">
                                <i class="i i-arrow-right"></i>
                            </a>
                            <a href="#notes" data-toggle="class:show animated fadeInRight">Ver todas tus
                                notificaciones</a>
                        </footer>
                    </section>
                </section>
            </li>
            <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span
                    class="thumb-sm avatar pull-left"> <img src="images/a0.jpg"> </span> John Smith <b
                    class="caret"></b> </a>
                <ul class="dropdown-menu animated fadeInRight m-t-xs">
                    <span class="arrow top hidden-nav-xs"></span>
                    <li><a href="#"> <span class="badge bg-danger pull-right">3</span> Notificaciones </a></li>
                    <li><a href="profile.php">Perfil</a></li>
                    <li><a href="#">Configuración</a></li>
                    <li><a href="docs.html">Ayuda</a></li>
                    <li class="divider"></li>
                    <li><a href="signin.php" >Cerrar Sesión</a></li>
                </ul>
            </li>
        </ul>
        <!-- fin notificaciones desplegables y perfil -->
    </header>
    <!-- fin menú superior  -->
    <section>
        <section class="hbox stretch">
            <!-- .aside -->
            <aside class="bg-black aside-md hidden-print" id="nav">
                <section class="vbox">
                    <section class="w-f scrollable">
                        <div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0"
                             data-size="10px" data-color="#333333">
                            <div class="clearfix wrapper dk nav-user hidden-xs">
                                <!-- perfil desplegable  -->
                                <div class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <span class="thumb avatar pull-left m-r">
                                                <img src="images/a0.jpg">
                                                <i class="on md b-black"></i>
                                            </span>
                                        <span class="hidden-nav-xs clear">
                                                <span class="block m-t-xs">
                                                    <strong class="font-bold text-lt">John Smith</strong>
                                                    <b class="caret"></b>
                                                </span>
                                            </span>
                                    </a>
                                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                        <span class="arrow top hidden-nav-xs"></span>
                                        <li><a href="#"> <span class="badge bg-danger pull-right">3</span>
                                            Notificaciones </a></li>
                                        <li><a href="profile.php">Perfil</a></li>
                                        <li><a href="#">Configuración</a></li>
                                        <li><a href="docs.html">Ayuda</a></li>
                                        <li class="divider"></li>
                                        <li><a href="signin.php" >Cerrar Sesión</a></li>
                                    </ul>
                                </div>
                                <!-- fin perfil desplegable  -->
                            </div>
                            <!-- menú lateral -->
                            <nav class="nav-primary hidden-xs">
                                <div class="text-muted text-sm hidden-nav-xs padder m-t-sm m-b-sm">Inicio</div>
                                <ul class="nav nav-main" data-ride="collapse">
                                    <li class="active">
                                        <a href="index.php" class="auto">
                                            <i class="i i-music icon"> </i>
                                            <span class="font-bold">Canciones Populares</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="new-artists.php" class="auto">
                                            <i class="i i-star2 icon"> </i>
                                            <span class="font-bold">Nuevos artistas</span> </a>
                                    </li>
                                    <li><a href="#" class="auto">
                                        <i class="i i-vynil icon"> </i>
                                        <span class="font-bold">Próximos lanzamientos</span>
                                    </a>
                                    </li>
                                    <li>
                                        <a href="#" class="auto">
                                            <i class="i i-clock icon"> </i>
                                            <span class="font-bold">Próximos conciertos</span>
                                        </a>
                                    </li>
                                </ul>

                                <!-- fin menú lateral -->
                            </nav>
                        </div>

                    </section>
                    <!-- logout inferior -->
                    <footer class="footer hidden-xs no-padder text-center-nav-xs">
                        <a href="signin.php"
                           class="btn btn-icon icon-muted btn-inactive pull-right m-l-xs m-r-xs hidden-nav-xs">
                            <i class="i i-logout"></i>
                        </a>
                        <a href="#nav" data-toggle="class:nav-xs"
                           class="btn btn-icon icon-muted btn-inactive m-l-xs m-r-xs">
                            <i class="i i-circleleft text"></i>
                            <i class="i i-circleright text-active"></i>
                        </a>
                    </footer>
                    <!-- fin logout inferior -->
        </section>
      </aside>
      <!-- /.aside -->
      <section id="content">
        <section class="vbox">
          <section class="scrollable">
            <section class="hbox stretch">
                <section class="vbox">
                  <section class="scrollable">
                    <div class="wrapper">
                      <section class="panel no-border bg-primary lt">
                        <div class="panel-body">
                          <div class="row m-t-xl">
                            <div class="col-xs-3 text-right padder-v">

                            </div>
                            <div class="col-xs-6 text-center">
                              <div class="inline">
                                <div class="easypiechart" data-percent="100" data-line-width="8" data-bar-color="#fff" data-track-Color="#0d5e92" data-scale-Color="false" data-size="140" data-line-cap='butt' data-animate="1000">
                                    <div class="thumb-lg" class> 
                                              <?php
                                                     $consulta= "SELECT * FROM album Where id_album=$id_album";
                                                     $resultado = $conexion->query($consulta);
                                                     $row = $resultado->fetch_assoc()
                                                     ?>
                                                         <img class="img-circle" src ="data:image/jpg;base64,<?php echo base64_encode($row['portada']);?>"
                                    </div>
                                </div>
                                <div class="h4 m-t m-b-xs font-bold text-lt"><?php echo $row['nombre'];?>
                                    </div>
                                <small class="text-muted m-b"></small> </div>
                            </div>
                            <div class="col-xs-3 padder-v">
                            </div>
                          </div>
                          <div class="wrapper m-t-xl m-b">
                            <div class="row m-b">
                              <div class="col-xs-6 text-right"> <small>Año</small>
                                <div class="text-lt font-bold"> <?php echo $row['anio'];?>
                                  </div>
                              </div>
                              <div class="col-xs-6"> <small>Label</small>
                                <div class="text-lt font-bold"> <?php echo $row['label'];?>
                                  </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-xs-6 text-right"> <small>Géneros</small>
                                <div class="text-lt font-bold"><?php echo $row['genero'];?>
                                  </div>
                              </div>
                                 
                                <div class="col-xs-6"> <small>Interpretes y colaboradores</small>
                                    <?php
                                    $consulta= "SELECT x.nombre FROM artista as x JOIN album as y ON id_artista = interprete where id_album=$id_album";
                                    $resultado = $conexion->query($consulta);
                                    $row2 = $resultado->fetch_assoc()
                                    ?>

                                <div class="text-lt font-bold"><?php echo $row2['nombre'];?>
                                    </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <footer class="panel-footer dk text-center no-border">
                          <div class="row pull-out">
                            <div class="col-xs-4">
                              <div class="padder-v"> <span class="m-b-xs h3 block text-white">6</span> <small class="text-muted">Likes</small> </div>
                            </div>
                            <div class="col-xs-4 dker">
                              <div class="padder-v"> <span class="m-b-xs h3 block text-white">18</span> <small class="text-muted">Canciones en total</small> </div>
                            </div>
                            <div class="col-xs-4">
                              <div class="padder-v"> <span class="m-b-xs h3 block text-white">4</span> <small class="text-muted">Videoclips</small> </div>
                            </div>
                          </div>
                        </footer>
                      </section>
                    </div>
                  </section>
                </section>
              </aside>
              <aside class="col-lg-9 b-l no-padder">
                <section class="vbox">
                  <section class="scrollable">
                    <div class="wrapper">

                      <section class="panel panel-default">
                        <h4 class="padder">Última actividad</h4>
                        <ul class="list-group">
                          <li class="list-group-item">
                            <p>Presentación en concierto de estados unidos </p>
                            <small class="block text-muted"><i class="fa fa-clock-o"></i> Hace 1 días</small> </li>
                          <li class="list-group-item">
                            <p>The Weeknd lanza dos temas en colaboración con Daft Punk</p>
                            <small class="block text-muted"><i class="fa fa-clock-o"></i> Hace 2 semanas</small> </li>
                          <li class="list-group-item">
                            <p>Lanzamiento oficial del video "I Feel It Coming" junto a Daft Punk </p>
                            <small class="block text-muted"><i class="fa fa-clock-o"></i> Hace 1 semana</small> </li>
                        </ul>
                      </section>
                      <section class="panel clearfix bg-info youtube">
                        <div class="panel-body"> <a href="#" class="thumb pull-left m-r"> <img src="data:image/jpg;base64,<?php echo base64_encode($row['portada']);?>" class="img-circle b-a b-3x b-white"> </a>
                          <div class="clear"> <a href="#" class="text-info"><?php echo $row2['nombre'];?> - <?php echo $row['nombre'];?> <i class="fa fa-youtube"></i></a> <small class="block text-muted">13M de seguidores / 30 videos subidos</small> <a href="#" class="btn btn-xs btn-info2 m-t-xs">Suscribirse</a> </div>
                        </div>
                      </section>
                        <section class="panel clearfix bg-info soundcloud">
                        <div class="panel-body"> <a href="#" class="thumb pull-left m-r"> <img src="data:image/jpg;base64,<?php echo base64_encode($row['portada']);?>" class="img-circle b-a b-3x b-white"> </a>
                          <div class="clear"> <a href="#" class="text-info"><?php echo $row2['nombre'];?> - <?php echo $row['nombre'];?> <i class="i i-cloud"></i></a> <small class="block text-muted">5,94M de seguidores</small> <a href="#" class="btn btn-xs btn-info3 m-t-xs">Seguir</a> </div>
                        </div>
                      </section>
                    </div>
                  </section>
                </section>
              </aside>
            </section>
          </section>
        </section>
        <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a> </section>
    </section>
  </section>
</section>
<!-- Bootstrap -->
<!-- App -->
<script src="js/app.v1.js"></script>
<script src="js/charts/easypiechart/jquery.easy-pie-chart.js"></script>
<script src="js/app.plugin.js"></script>
</body>
</html>