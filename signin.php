<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Ingresar</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximun-scale=1, minimum-scale=1">
        <link rel="stylesheet" href="css/login.css">
        <link rel="shortcut icon" type="image/png" href="images/favicon.png"/>
    </head>
    <body>
        <form  action="validarusuario.php" method="post" class="formulario-registro">
            <h2 class="formulario-titulo">Ingrese Usuario</h2>
            <div class="contenedor-inputs">
            <input type="text" placeholder="&#127911; Nombre de Usuario" name="apodo" required>
            <input type="password" placeholder="&#127929; Contraseña" name="contrasena" required>
                <?php
                if(isset($_GET["fallo"])&& $_GET["fallo"]=='true'){
                echo"<div style='color:red'> Usuario o Contraseña incorrectos</div>"; }
                ?>
            <input type= "submit" value="Ingresar" href="index.php" >
            <a></a>
            <p class="form__link">¿Eres nuevo? <a href="signup.html">Regístrate aquí</a></p>
            </div>
        </form>
    </body>
</html>