<?php 
include("conexion.php");
?>
<!DOCTYPE html>
<html lang="en" class="app">
<?php
include("menu.php");
?>
            <section id="content">
                <section class="vbox">
                    <section class="scrollable">
                        <section class="hbox stretch">
                            <section class="vbox">
                                <section class="scrollable">
                                    <div class="wrapper">
                                        <section class="centro">
                                            <div class="flexslider">
                                                <ul class="slides">
                                                    <li>
                                                        <img src="images/monstercat.jpg"/>
                                                    </li>
                                                    <li>
                                                        <img src="images/bts.jpeg"/>
                                                    </li>
                                                    <li>
                                                        <img src="images/monstercat.jpg"/>
                                                    </li>
                                                    <li>
                                                        <img src="images/bts.jpeg"/>
                                                    </li>
                                                </ul>
                                            </div>
                                        </section>

                                        <div>
                                            <link rel="stylesheet" href="css/galeria.css">
                                            <h1 class="titulo-1" >Álbumes recientes</h1>
                                            <ul class="galeria">
                                                     <?php
                                                     $consulta= "SELECT * FROM album";
                                                     $resultado = $conexion->query($consulta);
                                                     while($row = $resultado->fetch_assoc()){
                                                     ?>
                                                     <li > <a  href="album-profile.php?id=<?php echo $row['id_album'];?>">
                                                         <img src ="data:image/jpg;base64,<?php echo                                    base64_encode($row['portada']);?>"     </a>  </li>
                                                     <?php
                                                     }
                                                     ?>   
                                                    
                                            </ul>                  
                                        </div>
                                        
                                        <div>
                                            <link rel="stylesheet" href="css/galeria.css">
                                            <h1 class="titulo-1" >Artistas recientes</h1>
                                            <ul class="galeria">        
                                                     <?php
                                                     $consulta= "SELECT * FROM artista";
                                                     $resultado = $conexion->query($consulta);
                                                     while($row = $resultado->fetch_assoc()){
                                                     ?>
                                                     <li ><a  href="artist-profile.php?id=<?php echo $row['id_artista'];?>">
                                                         <img src ="data:image/jpg;base64,<?php echo                                    base64_encode($row['portada']);?>"       </a></li>
                                                     <?php
                                                     }
                                                     ?>   
                                                    </a>
                                            </ul>                  
                                        </div>
                                    </div>
                                </section>
                            </section>
                        </section>
                        </div>
                    </section>
                </section>
                </div>
            </section>
            <!-- fin contenido -->
        </section>
    </section>
</section>
<?php
include("footer.php");
?>
</html>