<?php
include("conexion.php");
?>
<!DOCTYPE html>
<html lang="en" class="app">
<?php
include("menu.php");
?>
<section id="content">
    <section class="vbox">
        <section class="scrollable">
            <section class="hbox stretch">
                <section class="vbox">
                    <section class="scrollable">
                        <div class="wrapper">

                            <link rel="stylesheet" href="css/formulario.css">
                            <form action="addAlbum.php" method="post" enctype="multipart/form-data"  class="formulario-registro">
                                <h2 class="formulario-titulo">Agregar Álbum </h2>
                                <div class="contenedor-inputs">
                                    <input type="text" placeholder="&#127901; Nombre" name="nombre" class="input-48" required>
                                    <input type="text" placeholder="&#127901; Género" name="genero" class="input-48" required>
                                    <input type="text" placeholder="&#127901; Disquera" name="label" class="input-48" required>
                                    <input type="text" placeholder="&#127901; Año" name="anio" class="input-48" required>
                                    <input type="text" placeholder="&#127901; Sitio Web" name="sitio_web" class="input-100" required>
                                    <input type="text" placeholder="&#127901; Tipo Álbum " name="tipo_album" class="input-100" required>
                                    <input type="text" placeholder="&#127901; Intérprete" name="interprete" class="input-100" required>
                                    <input type="file" placeholder="&#127911; Imagen" name="portada" class="input-100" required>
                                    <?php
                                    if(isset($_GET["fallo"])&& $_GET["fallo"]=='true'){
                                        echo"<div style='color:red'> Datos incorrectos, Año e Interprete deben ser valores enteros</div>"; }
                                    ?>
                                    <input type= "submit" value="Subir a la página">
                                </div>
                            </form>
                        </div>
                    </section>
                </section>
            </section>
            </div>
        </section>
    </section>
    </div>
</section>
<!-- fin contenido -->
</section>
</section>
</section>
<?php
include("footer.php");
?>
</html>