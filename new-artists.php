<?php
include("conexion.php");
?>
<!DOCTYPE html>
<html lang="en" class="app">
<?php
include("menu.php");
?>
            <section id="content">
                <section class="vbox">
                    <section class="scrollable">
                        <section class="hbox stretch">
                            <section class="vbox">
                                <section class="scrollable">
                                    <div class="wrapper">

                                        <link rel="stylesheet" href="css/galeria.css">
                                            <h1 class="titulo-1" >Nuevos Artistas</h1>
                                            <ul class="galeria">        
                                                     <?php
                                                     $consulta= "SELECT * FROM artista ORDER BY id_artista DESC LIMIT 6";
                                                     $resultado = $conexion->query($consulta);
                                                     while($row = $resultado->fetch_assoc()){
                                                     ?>
                                                     <li ><a  href="artist-profile.php?id=<?php echo $row['id_artista'];?>">
                                                         <img src ="data:image/jpg;base64,<?php echo                                    base64_encode($row['portada']);?>"       </a></li>
                                                     <?php
                                                     }
                                                     ?>   
                                                    </a>
                                            </ul>         

                                    </div>
                                </section>
                            </section>
                        </section>
                        </div>
                    </section>
                </section>
                </div>
            </section>
            <!-- fin contenido -->
        </section>
    </section>
</section>
<?php
include("footer.php");
?>
</html>