<?php
?>
<head>
    <meta charset="utf-8"/>
    <title>WikiMusic</title>
    <meta name="description"
          content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <link rel="stylesheet" href="css/app.v1.css" type="text/css"/>
    <link rel="stylesheet" href="js/calendar/bootstrap_calendar.css" type="text/css"/>
    <link rel="stylesheet" href="js/calendar/bootstrap_calendar.css" type="text/css"/>
    <link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen"/>
    <!--[if lt IE 9]>
    <script src="js/ie/html5shiv.js"></script>
    <script src="js/ie/respond.min.js"></script>
    <script src="js/ie/excanvas.js"></script> <![endif]-->
    <link rel="shortcut icon" type="image/png" href="images/favicon.png"/>
</head>
<body class="">
<section class="vbox">
    <header class="bg-white header header-md navbar navbar-fixed-top-xs box-shadow">
        <!-- menú superior  -->
        <div class="navbar-header aside-md dk"><a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen"
                                                  data-target="#nav"> <i class="fa fa-bars"></i> </a>
            <a href="index.php" class="navbar-brand"><img src="images/logo.png" class="m-r-sm">WikiMusic</a> <a
                class="btn btn-link visible-xs" data-toggle="dropdown" data-target=".user"> <i class="fa fa-cog"></i>
        </a></div>
        <ul class="nav navbar-nav hidden-xs">
            <!-- menú de puntitos desplegable -->
        </ul>
        <!-- fin menú de puntitos desplegable -->
        <form class="navbar-form navbar-left input-s-lg m-t m-l-n-xs hidden-xs" role="search">
            <!-- buscador -->
            <div class="form-group">
                <div class="input-group"> <span class="input-group-btn">
          <button type="submit" class="btn btn-sm bg-white b-white btn-icon"><i class="fa fa-search"></i></button>
          </span>
                    <input type="text" class="form-control input-sm no-border" placeholder="Busca artistas, estilos...">
                </div>
            </div>
        </form>
        <!-- fin buscador -->
        <ul class="nav navbar-nav navbar-right m-n hidden-xs nav-user user">
            <!-- notificaciones desplegables y perfil -->
            <li class="hidden-xs">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="i i-chat3"></i>
                    <span class="badge badge-sm up bg-danger count">3</span>
                </a>
                <section class="dropdown-menu aside-xl animated flipInY">
                    <section class="panel bg-white">
                        <header class="panel-heading b-light bg-light">
                            <strong>Tienes <span class="count">3</span> notificaciones</strong>
                        </header>
                        <div class="list-group list-group-alt">
                            <a href="#" class="media list-group-item">
                                    <span class="media-body block m-b-none">¡Dua Lipa ha subido un nuevo video!<br>
                                        <small class="text-muted">Hace 10 minutos</small>
                                    </span>
                            </a>
                            <a href="#" class="media list-group-item">
                                    <span class="media-body block m-b-none">Avicii ha anunciado un nuevo concierto<br>
                                        <small class="text-muted">Hace 1 hora</small>
                                    </span>
                            </a>
                            <a href="#" class="media list-group-item">
                                    <span class="media-body block m-b-none">¡Dua Lipa ha subido un nuevo video!<br>
                                        <small class="text-muted">Hace 10 minutos</small>
                                    </span>
                            </a>
                            <a href="#" class="media list-group-item">
                                    <span class="media-body block m-b-none">¡Dua Lipa ha subido un nuevo video!<br>
                                        <small class="text-muted">Hace 10 minutos</small>
                                    </span>
                            </a>
                        </div>
                        <footer class="panel-footer text-sm">
                            <a href="#" class="pull-right">
                                <i class="i i-arrow-right"></i>
                            </a>
                            <a href="#notes" data-toggle="class:show animated fadeInRight">Ver todas tus
                                notificaciones</a>
                        </footer>
                    </section>
                </section>
            </li>
            <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span
                    class="thumb-sm avatar pull-left"> <img src="images/a0.jpg"> </span> John Smith <b
                    class="caret"></b> </a>
                <ul class="dropdown-menu animated fadeInRight m-t-xs">
                    <span class="arrow top hidden-nav-xs"></span>
                    <li><a href="#"> <span class="badge bg-danger pull-right">3</span> Notificaciones </a></li>
                    <li><a href="profile.php">Perfil</a></li>
                    <li><a href="#">Configuración</a></li>
                    <li><a href="docs.html">Ayuda</a></li>
                    <li class="divider"></li>
                    <li><a onclick="cerrarSesion(this)" >Cerrar Sesión</a></li>
                </ul>
            </li>
        </ul>
        <!-- fin notificaciones desplegables y perfil -->
    </header>
    <!-- fin menú superior  -->
    <section>
        <section class="hbox stretch">
            <!-- .aside -->
            <aside class="bg-black aside-md hidden-print" id="nav">
                <section class="vbox">
                    <section class="w-f scrollable">
                        <div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0"
                             data-size="10px" data-color="#333333">
                            <div class="clearfix wrapper dk nav-user hidden-xs">
                                <!-- perfil desplegable  -->
                                <div class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <span class="thumb avatar pull-left m-r">
                                                <img src="images/a0.jpg">
                                                <i class="on md b-black"></i>
                                            </span>
                                        <span class="hidden-nav-xs clear">
                                                <span class="block m-t-xs">
                                                    <strong class="font-bold text-lt">John Smith</strong>
                                                    <b class="caret"></b>
                                                </span>
                                            </span>
                                    </a>
                                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                        <span class="arrow top hidden-nav-xs"></span>
                                        <li><a href="#"> <span class="badge bg-danger pull-right">3</span>
Notificaciones </a></li>
                                        <li><a href="profile.php">Perfil</a></li>
                                        <li><a href="#">Configuración</a></li>
                                        <li><a href="docs.html">Ayuda</a></li>
                                        <li class="divider"></li>
                                        <li><a onclick="cerrarSesion(this)">Cerrar Sesión</a></li>
                                    </ul>
                                </div>
                                <!-- fin perfil desplegable  -->
                            </div>
                            <!-- menú lateral -->
                            <nav class="nav-primary hidden-xs">
                                <div class="text-muted text-sm hidden-nav-xs padder m-t-sm m-b-sm">Inicio</div>
                                <ul class="nav nav-main" data-ride="collapse">
                                    <li class="active">
                                        <a href="index.php" class="auto">
                                            <i class="i i-music icon"> </i>
                                            <span class="font-bold">Canciones Populares</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="new-artists.php" class="auto">
                                            <i class="i i-star2 icon"> </i>
                                            <span class="font-bold">Nuevos artistas</span> </a>
                                    </li>
                                    <li><a href="#" class="auto">
                                        <i class="i i-vynil icon"> </i>
                                        <span class="font-bold">Próximos lanzamientos</span>
                                    </a>
                                    </li>
                                    <li>
                                        <a href="#" class="auto">
                                            <i class="i i-clock icon"> </i>
                                            <span class="font-bold">Próximos conciertos</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="addAlbumFront.php" class="auto">
                                            <i class="i i-plus2 icon"> </i>
                                            <span class="font-bold">Añadir Álbumes</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="addArtistFront.php" class="auto">
                                            <i class="i i-plus2 icon"> </i>
                                            <span class="font-bold">Añadir Artistas</span>
                                        </a>
                                    </li>
                                </ul>

                                <!-- fin menú lateral -->
                            </nav>
                        </div>

                    </section>
                    <!-- logout inferior -->
                    <footer class="footer hidden-xs no-padder text-center-nav-xs">
                        <a onclick="cerrarSesion(this)"
                           class="btn btn-icon icon-muted btn-inactive pull-right m-l-xs m-r-xs hidden-nav-xs">
                            <i class="i i-logout"></i>
                        </a>
                        <a href="#nav" data-toggle="class:nav-xs"
                           class="btn btn-icon icon-muted btn-inactive m-l-xs m-r-xs">
                            <i class="i i-circleleft text"></i>
                            <i class="i i-circleright text-active"></i>
                        </a>
                    </footer>
                    <!-- fin logout inferior -->
                </section>
            </aside>
            <!-- /.aside -->