<?php
include("conexion.php");
?>
<!DOCTYPE html>
<html lang="en" class="app">
<?php
include("menu.php");
?>
<section id="content">
    <section class="vbox">
        <section class="scrollable">
            <section class="hbox stretch">
                <section class="vbox">
                    <section class="scrollable">
                        <div class="wrapper">
                            <form action="addArtist.php" method="post" enctype="multipart/form-data"  class="formulario-registro">

                                <h2 class="formulario-titulo">Agregar Artista </h2>

                                <div class="contenedor-inputs">

                                <link rel="stylesheet" href="css/formulario.css">
                                    <link rel="shortcut icon" type="image/png" href="images/favicon.png"/>

                                    <input type="text" placeholder="&#127908; Nombre" name="nombre" class="input-48" required>

                            <input type="text" placeholder="&#127908; Edad" name="edad" class="input-48" required>

                            <input type="text" placeholder="&#127908; País" name="pais" class="input-48" required>

                            <input type="text" placeholder="&#127908; Género" name="genero" class="input-48" required>

                            <input type="text" placeholder="&#127908; Sitio Web" name="sitio_web" class="input-100" required>

                            <input type="file" placeholder="&#127908; Imagen" name="portada" class="input-100" required>
                                    <?php
                                    if(isset($_GET["fallo"])&& $_GET["fallo"]=='true'){
                                        echo"<div style='color:red'> Datos incorrectos, la edad debe ser un valor entero</div>"; }
                                    ?>
                            <input type= "submit" value="Subir a la página">

                        </div>

                        </form>

                        </div>
                    </section>
                </section>
            </section>
            </div>
        </section>
    </section>
    </div>
</section>
<!-- fin contenido -->
</section>
</section>
</section>
<?php
include("footer.php");
?>
</html>